package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.GregorianCalendar;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Utilisateur;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class UtilisateurRepositoryTest {

	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	@Test
	public void save() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setUsername("user2");
		utilisateur.setLastname("last2");
		utilisateur.setFirstname("first2");
		utilisateur.setGender("Female");
		utilisateur.setBirthdate(new GregorianCalendar(2002, 6, 29).getTime());
		
		utilisateurRepository.save(utilisateur);
		
		assertThat(utilisateur.getId()).isGreaterThan(0);
	}
}
