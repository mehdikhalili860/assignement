package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VersementRepositoryTest {

	@Autowired
	private VersementRepository versementRepository;
	
	@Test
	public void save() {
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setUsername("user2");
		utilisateur.setLastname("last2");
		utilisateur.setFirstname("first2");
		utilisateur.setGender("Female");
		utilisateur.setBirthdate(new GregorianCalendar(2002, 6, 29).getTime());
		
		Compte compte = new Compte();
		compte.setNrCompte("010000B025001000");
		compte.setRib("RIB2");
		compte.setSolde(BigDecimal.valueOf(140000L));
		compte.setUtilisateur(utilisateur);
		
		Versement versement = new Versement();
		versement.setMotifVersement("Assignment 2021");
		versement.setMontantVirement(BigDecimal.TEN);
		versement.setDateExecution(new Date());
		versement.setCompteBeneficiaire(compte);
		versement.setNom_prenom_emetteur("Mehdi Khalili");
		versement.getCompteBeneficiaire().setSolde(versement.getCompteBeneficiaire().getSolde().add(versement.getMontantVirement()));
		
		versementRepository.save(versement);
		
		assertThat(versement.getId()).isGreaterThan(0);
	}
}
