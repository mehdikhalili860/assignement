package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.util.EventType;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class AuditVirementRepositoryTest {
	@Autowired
	private AuditVirementRepository auditVirementRepository;

	@Test
	public void save() {
		AuditVirement auditVirement = new AuditVirement();
		auditVirement.setEventType(EventType.VERSEMENT);
		
		auditVirementRepository.save(auditVirement);
		
		assertThat(auditVirement.getId()).isGreaterThan(0);
	}
}
