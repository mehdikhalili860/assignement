package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;

  @Test
  public void findOne() {
  }

  @Test
  public void findAll() {

  }

  @Test
  public void save() {
	Utilisateur utilisateur1 = new Utilisateur();
	utilisateur1.setUsername("user1");
	utilisateur1.setLastname("last1");
	utilisateur1.setFirstname("first1");
	utilisateur1.setGender("Male");
	utilisateur1.setBirthdate(new GregorianCalendar(1999, 4, 24).getTime());

	Utilisateur utilisateur2 = new Utilisateur();
	utilisateur2.setUsername("user2");
	utilisateur2.setLastname("last2");
	utilisateur2.setFirstname("first2");
	utilisateur2.setGender("Female");
	utilisateur2.setBirthdate(new GregorianCalendar(2002, 6, 29).getTime());
	
	Compte compte1 = new Compte();
	compte1.setNrCompte("010000A000001000");
	compte1.setRib("RIB1");
	compte1.setSolde(BigDecimal.valueOf(200000L));
	compte1.setUtilisateur(utilisateur1);

	Compte compte2 = new Compte();
	compte2.setNrCompte("010000B025001000");
	compte2.setRib("RIB2");
	compte2.setSolde(BigDecimal.valueOf(140000L));
	compte2.setUtilisateur(utilisateur2);

	Virement virement = new Virement();
	virement.setMontantVirement(BigDecimal.TEN);
	virement.setCompteBeneficiaire(compte2);
	virement.setCompteEmetteur(compte1);
	virement.setDateExecution(new Date());
	virement.setMotifVirement("Assignment 2021");
	
	virementRepository.save(virement);
	
	assertThat(virement.getId()).isGreaterThan(0);
  }

  @Test
  public void delete() {
  }
}