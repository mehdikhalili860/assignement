package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.util.EventType;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class AuditVersementRepositoryTest {
	
	@Autowired
	private AuditVersementRepository auditVersementRepository;

	@Test
	public void save() {
		AuditVersement auditVersement = new AuditVersement();
		auditVersement.setEventType(EventType.VERSEMENT);
		
		auditVersementRepository.save(auditVersement);
		
		assertThat(auditVersement.getId()).isGreaterThan(0);
	}
}
