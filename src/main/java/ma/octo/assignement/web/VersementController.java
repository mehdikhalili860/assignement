package ma.octo.assignement.web;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AutiService;

@RestController(value = "/versements")
public class VersementController {

	Logger LOGGER = LoggerFactory.getLogger(VersementController.class);
	
	@Autowired
    private CompteRepository compteRepository;
	@Autowired
	private VersementRepository versementRepository;
	@Autowired
    private AutiService monservice;
    
    @GetMapping("lister_versements")
    List<Versement> loadAll() {
        List<Versement> all = versementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }
    
    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
    	
        Compte compte = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());
        
        if (compte == null) {
        	String message = "Compte Non existant";
            System.out.println(message);
            throw new CompteNonExistantException(message);
        }

        if (versementDto.getMontantVirement().equals(null) || versementDto.getMontantVirement().intValue() == 0) {
        	String message = "Montant vide";
            System.out.println(message);
            throw new TransactionException(message);
        } else if (versementDto.getMontantVirement().intValue() < 10) {
        	String message = "Montant minimal de versement non atteint";
            System.out.println(message);
            throw new TransactionException(message);
        }

        if (versementDto.getMotif().length() <= 0) {
        	String message = "Motif vide";
            System.out.println(message);
            throw new TransactionException(message);
        }

        compte.setSolde(new BigDecimal(compte.getSolde().intValue() + versementDto.getMontantVirement().intValue()));
        compteRepository.save(compte);

        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());
        versement.setCompteBeneficiaire(compte);
        versement.setNom_prenom_emetteur("Mehdi Khalili");
        versement.setMontantVirement(versementDto.getMontantVirement());

        versementRepository.save(versement);

        monservice.auditVirement("Versement depuis " + versementDto.getNomEmetteur() + " vers " + versementDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVirement()
                        .toString());
    }
    
}
